
#include <asf.h>
#include <avr\io.h>
#include <avr\interrupt.h>
#include <avr\delay.h>
#define FOSC 8000000UL
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

SIGNAL(USART_RXC_vect)
{
	char data;
	data = UDR;
}

unsigned char USART_Recieve(void)
{
	while( !(UCSRA & (1 << RXC)) );
	
	return UDR;

}

void USART_Init(unsigned int ubrr)
{
	//set baud rate
	UBRRH =	(unsigned char)(ubrr>>8);
	UBRRL = (unsigned char)ubrr;
	
	//enable reciever and transmitter and interupt
	UCSRB |= (1<<RXEN);
	//set format: 8data, 2 stop bit
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);
}

void lcd_command(unsigned char command)
{
	PORTB &=~(1<<5); //Rs low
	PORTA = command;
	PORTB |= (1<<4); //E high
	PORTB &= ~(1<<4); //E low
	_delay_ms(5);
}

void lcd_init()
{
	lcd_command(0x38); //8 bit mode, 5X8
	lcd_command(0x0c);
	lcd_command(0x06);
	lcd_command(0x01);
}

void lcd_display(unsigned char display)
{
	PORTB |= (1<<5);
	PORTA = display;
	PORTB |=(1<<4);
	PORTB &= ~(1<<4);
	_delay_ms(1);

}


void lcd_puts(char* str)
{
	while(*str)
	lcd_display(*(str++));
	
}



int main (void)
{
	cli(); // clear global interupt
	
	
	
	USART_Init(MYUBRR);
	sei();

	int keepRunning = 1;
	char nrCifreChar = USART_Recieve();
	int nrCifreInt = (int)nrCifreChar - 48;
	char text[9];
	int i = 0;
	while (keepRunning == 1)
	{
		text[i] = USART_Recieve();
		i++;
		if(i == nrCifreInt)
		keepRunning = 0;
	}
	

	
	DDRB = 0xFF;
	DDRA = 0xFF;
	lcd_init();
lcd_command(0x83);
lcd_puts(text);
//lcd_display(text);
	while (1){
		
			
			
	}
	
	return 0;
	
}
