
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/delay.h>
#include <string.h>
#include <stdio.h>

#define FOSC 8000000UL
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1

SIGNAL(USART_RXC_vect)
{
	char data;
	data = UDR;
}

void USART_Transmit(unsigned char data)
{
while ( !( UCSRA & (1<<UDRE)) );
/* Copy 9th bit to TXB8 */
UCSRB &= ~(1<<TXB8);
if ( data & 0x0100 )
UCSRB |= (1<<TXB8);
/* Put data into buffer, sends the data */
UDR = data;
}

void USART_Init( unsigned int ubrr )
{
	/* Set baud rate */
	UBRRH = (unsigned char)(ubrr>>8);
	UBRRL = (unsigned char)ubrr;
	/* Enable receiver and transmitter */
	UCSRB = (1<<TXEN);
	/* Set frame format: 8data, 2stop bit */
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);
}

int main (void)
{
//cli(); // clear global interrupt flag
USART_Init ( MYUBRR );
//sei(); // set global interrupt flag
char* text = "Am intarziat cu tema?";
char c;
int intrat = 1;
//char c = text[1];
int integer = strlen(text);
//USART_Transmit('e');
char deTrimis = integer + '0';
USART_Transmit(deTrimis);
while (1) {
	
	if(intrat == 1)
	{
		
	
	for(int i = 0; i < strlen(text); i++)
	{
		c = text[i];
		if(c != NULL)
		{
			USART_Transmit(c);
		}
		
	}
	intrat = 0;
		
	}
	
	
}
return 0;
}
